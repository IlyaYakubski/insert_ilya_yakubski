INSERT INTO film (title, description, release_year, language_id, rental_duration, rental_rate, length, replacement_cost, rating, special_features)
VALUES ('THE TERMINATOR', 'A classic sci-fi action film', 1984, 1, 14, 4.99, 107, 19.99, 'R', '{Trailers, "Deleted Scenes"}');

INSERT INTO actor (first_name, last_name)
VALUES ('ARNOLD', 'SCHWARZENEGGER'), ('LINDA', 'HAMILTON'), ('MICHAEL', 'BIEHN');

INSERT INTO film_actor (actor_id, film_id)
SELECT actor_id, (SELECT film_id FROM film WHERE title = 'The Terminator') FROM actor
WHERE first_name IN ('ARNOLD', 'LINDA', 'MICHAEL');

INSERT INTO inventory (film_id, store_id, last_update)
VALUES ((SELECT film_id FROM film WHERE title = 'THE TERMINATOR'), 2, CURRENT_TIMESTAMP);